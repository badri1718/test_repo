 <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>

<meta charset="ISO-8859-1">

<title>Login</title>

<style>
	.failed{
		color:red
	}
	
</style>

</head>

<body>
	<h3>Please login Using Your Credentials</h3>

	<form:form action="${pageContext.request.contextPath}/authenticateTheUser" 
		method="POST">
		
		<!-- check for error message -->
	<c:if test="${param.error == null} ">
	
		<i>sorry! you have entered invalid username/password</i>
		
	</c:if>
	
	<p>
		User Name:<input type="text" name="username" />
	</p>
	<p>
		Password:<input type="password" name="password" />
	</p>
	<input type="submit" value="Login" />
	
	</form:form>
	
</body>

</html>